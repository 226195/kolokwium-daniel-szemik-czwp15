/**
 * 
 */
package kolos;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.util.Random;

/**
 * @author tb
 *
 */
public abstract class Figura implements Runnable, ActionListener {

	// wspolny bufor
	protected Graphics2D buffer;
	protected Area area;
	// do wykreslania
	protected Shape shape;
	// przeksztalcenie obiektu
	protected AffineTransform aft;

	// przesuniecie
	private int dx, dy;
	// rozciaganie
	private double sf;
	// kat obrotu
	private double an;
	private int delay;
	private int width;
	private int height;
	private Color clr;
	public static int nr=10;

	protected static final Random rand = new Random();
	public static int przes=1;

	public Figura(Graphics2D buf, int del, int w, int h) {
		delay = del;
		buffer = buf;
		width = w;
		height = h;

		clr = new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255), rand.nextInt(255));
	}

	@Override
	public void run() {
		aft.translate(nr, nr);
		area.transform(aft);
		shape = area;
		nr = nr + 50;

		while (true) {
			shape = nextFrame();

			try {
				Thread.sleep(delay);
			} catch (InterruptedException e) {
			}
		}
	}
	protected Shape nextFrame() {
		area = new Area(area);
		aft = new AffineTransform();
		aft.translate(nr, nr);
		// przeksztalcenie obiektu
		area.transform(aft);
		
		return area;
	}
	@Override
	public void actionPerformed(ActionEvent evt) {
		// wypelnienie obiektu
		buffer.setColor(clr.brighter());
		buffer.fill(shape);
		// wykreslenie ramki
		buffer.setColor(clr.darker());
		buffer.draw(shape);
		nr = nr + 50;
	}

}